# pyquickly

https://gitlab.com/paracpg/pyquickly.git

### Motiviation

Provide an example of multi-page dash plotly that provides an off the shelf multi-page environment for graph plotting.

### Provides:

A python library to assist with the generation of [dash plotly](https://github.com/plotly/dash) graph applications

### Project Status:  Concept!

### Getting Started Guide

At the moment I would recommend installation in developer mode ie:
```bash
$ python3 setup.py develop --user
```
Once the pyquickly library has been installed, and the various services are running in the environment (see below) you can start the app
```bash
$ cd examples
examples$ ./flaskapp.py
examples$ ./flaskapp.py
Dash is running on http://127.0.0.1:8050/content/

 * Serving Flask app 'flasksession'
 * Debug mode: off
 * Running on http://127.0.0.1:8050
Press CTRL+C to quit
 * Restarting with stat
127.0.0.1 - - [24/Dec/2022 13:56:34] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [24/Dec/2022 13:56:36] "POST / HTTP/1.1" 302 -

```
Once started direct your browser to the web page http://127.0.0.1:8050 type in your name and then press the **Press to continue** button to start the session and get redirected to the content page http://127.0.0.1:8050/content/.  Note if you do not create a session at http://127.0.0.1:8050 you will be directed to visit that page to create a session: Please **click on this link** (http://127.0.0.1:8050/) to create a session and enable this page.  Once the session is created you may open new browser tabs or create new windows and the session data will be preserved for you in the new browser tab or window.

Here are two examples of pages that are created by this application:

![the home page](example/HomePage.png)
Click on a point on the **Home** page plot to select the data to display on **tab-1** of the **Charts** page.

![the tabs page](example/TabsPage.png)

### Setting up your environment

I have mongodb (https://github.com/mongodb/mongo) running.  This application creates a mongo database and a couple of collections for some data and then reads that data and displays it on several pages provided by dash plotly.

At the time of writing I have not put any work into installation so the library and code are not guarenteed to work first time.  All I can say is that when I committed this code it work like a treat on my machine!

Please feel free to contact me if you find an issue or want to help.
