#!/usr/bin/env python3

from dash import Dash, dcc, html, Input, Output, State
import pyquickly.plotting as c

class D1(c.ChartPlotter):
    def __init__(self, shortname):
        c.ChartPlotter.__init__(self)
        self._shortname = shortname
        # layout unique name
        self._idname = 'D{}'.format(self.name)

        # fallback defaults
        TESTSET = 'sample_1'
        TESTNAME = 'sample_1'
        self.fallback_defaults = {'testkey': {'name': TESTSET}, 'testnames': [TESTNAME], 'testname': TESTNAME,
                                 'samplenames': ['sample', 'v', 'w', 'x', 'y', 'z'], 'xaxis': 'sample',
                                  TESTNAME: {'sample': [0,1], 'v': [0,0], 'w':[0,0], 'x': [0,0], 'y':[0,0], 'z':[0,0]}}

    @property
    def storename(self):
        return '{}store'.format(self.idname)
    @property
    def dbstorename(self):
        return '{}_session'.format(self.storename)

    @property
    def name(self):
        return self._shortname
    @property
    def idname(self):
        return self._idname
    @property
    def layout(self):
        toplot = self.define_plot(defaults = self.defaults, getdata=True)
        return html.Div([
            html.Button('Click me', id='btn_{}'.format(self.idname)),
            html.Div(id='output_{}'.format(self.idname)),
            dcc.Graph(id='chart{}'.format(self.idname), figure = c.create_fig(toplot, self.config, self.defaults)),
        ])

    def register_callbacks(self, app):
        @app.callback(Output(self.storename, 'data'), [
            State(self.storename, 'data'), Input('btn_{}'.format(self.idname), 'n_clicks')
        ], prevent_initial_call=True)
        def save_data(data, n_clicks):
            storename = self.dbstorename if 'session' in self.dbstorename else self.dbstorename + '_session'

            # use data from local dcc.Store or if data is empty recover from session (if available)
            toplot = self.create_update_sessionstore(data)
            if not 'testkey' in toplot:
                # likely the session data is empty so pick same default as when drawn
                toplot['testkey'] = self.defaults['testkey']

            if not 'samplenames' in toplot:
                testname = toplot['testname'] if 'testname' in toplot else None

                # likely the session data is empty so construct the same data as when drawn
                toplot = c.define_plot(toplot, self.config, testname, self.defaults)

            if n_clicks is None:
                # this seems to be only way to keep the total number of clicks recored
                # without the initial 'None' on 'n_clicks' on layout how else to update 'total'?
                toplot['total'] = toplot['n_clicks'] if 'n_clicks' in toplot else 0
            else:
                toplot['n_clicks'] = n_clicks + (toplot['total'] if 'total' in toplot else 0)

            return {storename: toplot}

        @app.callback(Output('output_{}'.format(self.idname), 'children'), [Input('session_store', 'data')])
        def from_saved(data):
            rtn = 0
            if self.storename in data:
                rtn = data[self.storename]['n_clicks'] if 'n_clicks' in data[self.storename] else 0
            elif self.dbstorename in data:
                rtn = data[self.dbstorename]['n_clicks'] if 'n_clicks' in data[self.dbstorename] else 0

            return 'btn_{} Clicked {} times'.format(self.idname, rtn)

        @app.callback(Output('chart{}'.format(self.idname), 'figure'), [Input('session_store', 'data')])
        def from_saved(data):
            storename = self.dbstorename if 'session' in self.dbstorename else self.dbstorename + '_session'

            # use data from local dcc.Store or if data is empty recover from session (if available)
            doc = data['homeplot_session'] if 'homeplot_session' in data else {}
            toplot = {}
            if 'selectedsample' in doc:
                toplot = {'testkey': {'name': doc['selectedsample']}}
                toplot = c.define_plot(toplot, self.config, doc['selectedsample'], self.defaults, getdata=True)

            if not 'testkey' in toplot:
                # likely the session data is empty so pick same default as when drawn
                toplot['testkey'] = self.defaults['testkey']

            if not 'samplenames' in toplot:
                testname = toplot['testname'] if 'testname' in toplot else None

                # likely the session data is empty so construct the same data as when drawn
                toplot = c.define_plot(toplot, self.config, testname, self.defaults)

            return c.create_fig(toplot, self.config, self.defaults)
