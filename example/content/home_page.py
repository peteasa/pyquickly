#!/usr/bin/env python3

import dash
from dash import Dash, dcc, html, Input, Output, State
import pyquickly.plotting as c

if not __name__== '__main__':
    try:
        dash.register_page(__name__, path='/',
                           title='Home page',
                           name='Home')
    except dash.exceptions.PageError:
        pass

class HomePlot(c.ChartPlotter):
    def __init__(self):
        c.ChartPlotter.__init__(self)

        # fallback defaults
        TESTSET = 'tset'
        TESTNAME = '4 sin'
        self.fallback_defaults = {'testkey': {'name': TESTSET}, 'testnames': [TESTNAME], 'testname': TESTNAME,
                                 'samplenames': ['sample', 'v', 'w', 'x', 'y', 'z'], 'xaxis': 'sample',
                                 TESTNAME: {'sample': [0,1], 'v': [0,0], 'w':[0,0], 'x': [0,0], 'y':[0,0], 'z':[0,0]}}

    @property
    def storename(self):
        return 'homeplot'
    @property
    def dbstorename(self):
        return '{}_session'.format(self.storename)

    @property
    def layout(self):
        toplot = self.define_plot(defaults = self.defaults, getdata=True)
        rtn = html.Div([
            dcc.Graph(id='{}chart'.format(self.__class__.__name__), figure = c.create_fig(toplot, self.config, self.defaults)),
        ])

        return rtn

    def register_callbacks(self, app):
        if 0:
            @app.callback(Output('output_home_navigation', 'children'),
                          [Input('url_home', 'href'), Input('url_home', 'pathname'), Input('url_home', 'search'), Input('url_home', 'hash')])
            def on_layout(href, pathname, search, hasht):
                return 'home_page: ' + pathname

        @app.callback(Output(self.storename, 'data'),
                      [State(self.storename, 'data'), Input('{}chart'.format(self.__class__.__name__), 'clickData')], prevent_initial_call=True)
        def graph_data_to_session(data, clickData):
            storename = self.dbstorename if 'session' in self.dbstorename else self.dbstorename + '_session'

            # use data from local dcc.Store or if data is empty recover from session (if available)
            toplot = self.create_update_sessionstore(data)
            if not 'testkey' in toplot:
                # likely the session data is empty so pick same default as when drawn
                toplot['testkey'] = self.defaults['testkey']

            if not 'samplenames' in toplot:
                testname = toplot['testname'] if 'testname' in toplot else None

                # likely the session data is empty so construct the same data as when drawn
                toplot = c.define_plot(toplot, self.config, testname, self.defaults)

            if not clickData is None:
                toplot['selectedlabel'] = c.id_to_label(clickData['points'][0]['curveNumber'], toplot)
                if 'hovertext' in clickData['points'][0]: toplot['selectedsample'] = clickData['points'][0]['hovertext']

            return {storename: toplot}

graph = HomePlot()
def config(doc):
    graph.config = doc

def layout():
    return graph.layout

def register_stores():
    return graph.register_stores

def register_callbacks(app):
    return graph.register_callbacks(app)

if __name__ == '__main__':
    # using custom style sheet https://dash.plotly.com/external-resources
    # folder ./static/ contains .css / .js files loaded in alphanumerical order by filename
    app = Dash(__name__, assets_folder='../static')

    app.layout = layout()

    register_callbacks(app)

    app.run_server(use_reloader=True, debug=True)
