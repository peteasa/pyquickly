#!/usr/bin/env python3

import dash
from dash import Dash, dcc, html, Input, Output
from pyquickly.graphdata import combine_dict

if __name__== '__main__':
    from overview_plot import Overview
    from detail_plot import Detail
else:
    from .overview_plot import Overview
    from .detail_plot import Detail

    try:
        dash.register_page(__name__, path='/multi',
                       title='Multi-plot page',
                       name='Multi')
    except dash.exceptions.PageError:
        pass


graphs = [ Overview(), Detail() ]
def config(doc):
    for graph in graphs:
        graph.config = doc

def register_stores():
    rtn = {}
    for graph in graphs:
        rtn = combine_dict(rtn, graph.register_stores)

    return rtn

def register_callbacks(app):
    for graph in graphs:
        graph.register_callbacks(app)

def layout():
    return html.Div([
            graphs[0].layout(style = {'width': '49%', 'display': 'inline-block', 'padding': '0 20'}),
            graphs[1].layout(style = {'width': '49%', 'display': 'inline-block'}),
    ])

if __name__ == '__main__':
    # using custom style sheet based on https://dash.plotly.com/external-resources
    # folder ./static/ contains .css / .js files loaded in alphanumerical order by filename
    app = Dash(__name__, assets_folder='../static')

    app.layout = layout()

    register_callbacks(app)

    app.run_server(debug=True, use_reloader=True)
