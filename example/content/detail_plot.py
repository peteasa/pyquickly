#!/usr/bin/env python3

import dash
from dash import Dash, dcc, html, Input, Output, State
from dash.exceptions import PreventUpdate
import pyquickly.plotting as c

class Detail(c.ChartPlotter):
    def __init__(self):
        c.ChartPlotter.__init__(self)

        # fallback defaults
        TESTSET = 'tset'
        TESTNAME = '4 sin'
        self.fallback_defaults = {'testkey': {'name': TESTSET}, 'testnames': [TESTNAME], 'testname': TESTNAME,
                                 'samplenames': ['sample', 'v', 'w', 'x', 'y', 'z'],
                                  TESTNAME: {'sample': [0,1], 'v': [0,0], 'w':[0,0], 'x': [0,0], 'y':[0,0], 'z':[0,0]},
                                  'xaxis': 'sample', 'yaxis': 'x', 'colour': None}

    @property
    def storename(self):
        return 'detail'
    @property
    def dbstorename(self):
        return '{}'.format(self.storename)

    def layout(self, style = None):
        toplot = self.define_plot(defaults = self.defaults, getdata=True)
        rtn = html.Div([
            dcc.Graph(id='{}_plot'.format(self.storename), figure = c.create_fig(toplot, self.config, self.defaults)),
            ], style=style,
        )

        return rtn

    def register_callbacks(self, app):
        @app.callback(Output('{}'.format(self.storename), 'data'), [State('{}'.format(self.storename), 'data'), Input('session_store', 'data')])
        def from_overview(data_local, data):
            if 'overview' in data:
                doc = data['overview']
            else: raise PreventUpdate

            local = data_local[self.storename] if self.storename in data_local else {}
            toplot = {}
            getdata = False
            if 'selectedsample' in doc and (
                    not ('testkey' in local and doc['selectedsample'] == local['testkey']['name'])):
                getdata = True
            if 'selectedlabel' in doc and (
                    not ('yaxis' in local and doc['selectedlabel'] == local['yaxis'])):
                getdata = True

            if getdata:
                toplot['testkey'] = {'name': doc['selectedsample']}
                toplot['testname'] = doc['selectedsample']
                if toplot['testname'] in local and 'testkey' in local and doc['selectedsample'] == local['testkey']['name']:
                    toplot['testnames'] = local['testnames']
                    toplot['testname'] = local['testname']
                    toplot['samplenames'] = local['samplenames']
                    toplot[local['testname']] = local[local['testname']]
                else:
                    # store the data in local store to avoid multiple database accesses
                    toplot = c.define_plot(toplot, self.config, toplot['testname'], self.defaults, getdata=True)

                toplot['xaxis'] = 'sample'
                toplot['yaxis'] = doc['selectedlabel'] if doc['selectedlabel'] in toplot['samplenames'] else None
                toplot['colour'] = None
            else: raise PreventUpdate

            return {self.storename: toplot}

        @app.callback(Output('{}_plot'.format(self.storename), 'figure'), [Input('{}'.format(self.storename), 'data')])
        def from_saved(data):
            if self.storename in data:
                toplot = data[self.storename]
            else: raise PreventUpdate

            return c.create_fig(toplot, self.config, self.defaults)

if __name__ == '__main__':
    # using custom style sheet https://dash.plotly.com/external-resources
    # folder ./static/ contains .css / .js files loaded in alphanumerical order by filename
    app = Dash(__name__, assets_folder='../static')

    graph = Detail()

    app.layout = graph.layout()

    #register_callbacks(app)

    app.run_server(use_reloader=True, debug=True)
