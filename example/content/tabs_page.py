#!/usr/bin/env python3

import dash
from dash import Dash, dcc, html, Input, Output
from flask import session
from pyquickly.graphdata import combine_dict

if __name__== '__main__':
    from tab_first import D1
    from tab_second import D2
    from tab_third import D3
else:
    from .tab_first import D1
    from .tab_second import D2
    from .tab_third import D3

    try:
        dash.register_page(__name__, path='/charts',
                       title='Charts page',
                       name='Charts')
    except dash.exceptions.PageError:
        pass


graphs = [ D1(1), D2(2), D3(3)]
def config(doc):
    for graph in graphs:
        graph.config = doc

def register_stores():
    rtn = {}
    for graph in graphs:
        rtn = combine_dict(rtn, graph.register_stores)

    return rtn

def register_callbacks(app):
    for graph in graphs:
        graph.register_callbacks(app)

def layout():
    return html.Div([
        dcc.Tabs(id='tabs', value='tab-1',
                 children=[dcc.Tab(label='tab-{}'.format(graph.name),
                                   value='tab-{}'.format(graph.name),
                                   children=[graph.layout]) for graph in graphs]),
        html.Div(id='tabs_store'),
    ])

if __name__ == '__main__':
    # using custom style sheet based on https://dash.plotly.com/external-resources
    # folder ./static/ contains .css / .js files loaded in alphanumerical order by filename
    app = Dash(__name__, assets_folder='../static')

    app.layout = layout()

    register_callbacks(app)

    app.run_server(debug=True, use_reloader=True)
