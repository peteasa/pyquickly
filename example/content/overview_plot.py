#!/usr/bin/env python3

import dash
from dash import Dash, dcc, html, Input, Output, State
import pyquickly.plotting as c

class Overview(c.ChartPlotter):
    def __init__(self):
        c.ChartPlotter.__init__(self)

        # fallback defaults
        TESTSET = 'tset'
        TESTNAME = '4 sin'
        self.fallback_defaults = {'testkey': {'name': TESTSET}, 'testnames': [TESTNAME], 'testname': TESTNAME,
                                 'samplenames': ['sample', 'v', 'w', 'x', 'y', 'z'],
                                  TESTNAME: {'sample': [0,1], 'v': [0,0], 'w':[0,0], 'x': [0,0], 'y':[0,0], 'z':[0,0]},
                                  'xaxis': 'sample', 'colour': None}

    @property
    def storename(self):
        return 'overview'
    @property
    def dbstorename(self):
        return '{}'.format(self.storename)

    def layout(self, style = None):
        toplot = self.define_plot(defaults = self.defaults, getdata=True)
        rtn = html.Div([
            dcc.Graph(
                id='{}_plot'.format(self.storename), figure = c.create_fig(toplot, self.config, self.defaults, logfn = self.logfn)),
            ], style=style,
        )

        return rtn

    def register_callbacks(self, app):
        @app.callback(Output(self.storename, 'data'),
                      [State(self.storename, 'data'), Input('{}_plot'.format(self.storename), 'hoverData')], prevent_initial_call=True)
        def graph_data_to_session(data, hoverData):
            storename = self.dbstorename

            toplot = self.create_update_sessionstore(data)
            if not 'testkey' in toplot:
                # likely the session data is empty so pick same default as when drawn
                toplot['testkey'] = self.defaults['testkey']

            if not 'samplenames' in toplot:
                testname = toplot['testname'] if 'testname' in toplot else None

                # likely the session data is empty so construct the same data as when drawn
                toplot = c.define_plot(toplot, self.config, testname, self.defaults, print if self.config['debug'] else None)

            if not hoverData is None:
                toplot['selectedlabel'] = c.id_to_label(hoverData['points'][0]['curveNumber'], toplot)
                if 'hovertext' in hoverData['points'][0]: toplot['selectedsample'] = hoverData['points'][0]['hovertext']

            return {storename: toplot}

if __name__ == '__main__':
    # using custom style sheet https://dash.plotly.com/external-resources
    # folder ./static/ contains .css / .js files loaded in alphanumerical order by filename
    app = Dash(__name__, assets_folder='../static')

    graph = Overview()

    app.layout = graph.layout()

    #register_callbacks(app)

    app.run_server(use_reloader=True, debug=True)
