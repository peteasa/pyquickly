#!/usr/bin/env python3

from dash import Dash, dcc, html, Input, Output, State

class D3(object):

    def __init__(self, shortname):
        self._shortname = shortname
        # layout unique name
        self._idname = 'D{}'.format(self.name)
        self._config = {}

    @property
    def storename(self):
        return '{}store'.format(self.idname)
    @property
    def dbstorename(self):
        return '{}_session'.format(self.storename)

    @property
    def name(self):
        return self._shortname
    @property
    def idname(self):
        return self._idname
    @property
    def layout(self):
        return html.Div([
            html.Button('Click me', id='btn_{}'.format(self.idname)),
            html.Div(id='output_{}'.format(self.idname)),
        ])

    @property
    def register_stores(self):
        rtn = {self.dbstorename: 'data'}

        return rtn

    @property
    def config(self):
        return self._config
    @config.setter
    def config(self, doc):
        self._config = doc

    def register_callbacks(self, app):
        @app.callback(Output(self.dbstorename, 'data'), [
            State(self.dbstorename, 'data'), Input('btn_{}'.format(self.idname), 'n_clicks')
        ], prevent_initial_call=True)
        def save_data(data, n_clicks):
            total_clicks = n_clicks
            rtn = {'D{}'.format(self.idname): total_clicks}
            if n_clicks is None:
                # initial clicks displayed is the initial stored value
                total_clicks = 0
                rtn = {}
                if 'D{}'.format(self.idname) in data and not data['D{}'.format(self.idname)] is None:
                    total_clicks += data['D{}'.format(self.idname)]
                    rtn = {'D{}'.format(self.idname): total_clicks}

            if 3 < total_clicks:
                rtn['D{}_session'.format(self.idname)] = total_clicks

            return rtn

        @app.callback(Output('output_{}'.format(self.idname), 'children'), [Input('session_store', 'data')])
        def from_saved(data):
            rtn = 0
            if 'D{}'.format(self.idname) in data:
                rtn = data['D{}'.format(self.idname)]
            elif 'D{}_session'.format(self.idname) in data:
                rtn = data['D{}_session'.format(self.idname)]

            return 'btn_{} Clicked {} times'.format(self.idname, rtn)
