#!/usr/bin/env python3

import dash
from dash import Dash, dcc, html, Input, Output, State, ctx
from dash.exceptions import PreventUpdate
import pyquickly.plotting as c

if not __name__== '__main__':
    try:
        dash.register_page(__name__, path='/3d',
                           title='3d page',
                           name='3d')
    except dash.exceptions.PageError:
        pass

class ThreeDPlot(c.ChartPlotter):
    def __init__(self):
        c.ChartPlotter.__init__(self, auxiliarynames = ['_id'])

        # dont plot
        dont_plot = []

        # make available for plotting via manual selection
        self.additional = ['v']

        # fallback defaults
        TESTSET = 'tset'
        TESTNAME = '4 sin'
        self.fallback_defaults = {'testkey': {'name': TESTSET}, 'testnames': [TESTNAME], 'testname': TESTNAME,
                                  'samplenames': ['sample1', 'v', 'x', 'y', 'z'] + dont_plot,
                                  'dont_plot': dont_plot + self.additional,
                                  TESTNAME: {'sample1': [0,1], 'v': [0,0], 'w':[0,0], 'x': [0,0], 'y':[0,0], 'z':[0,0]},
                                  'colour': None}

    @property
    def storename(self):
        return '3dplot'
    @property
    def dbstorename(self):
        return '{}_session'.format(self.storename)

    @property
    def layout(self):
        toplot = self.define_plot(defaults = self.defaults, getdata=True)
        rtn = html.Div([html.Div([dcc.Dropdown(toplot['testnames'], toplot['testname'], id='{}testname'.format(self.__class__.__name__)),], style={'width': '25%'}),
                        html.Div([
            html.Div([dcc.Dropdown(toplot['samplenames'] + self.additional, toplot['xaxis'] if 'xaxis' in toplot else None, id='{}xaxis'.format(self.__class__.__name__)),], style={'width': '25%'}),
            html.Div([dcc.Dropdown(toplot['samplenames'] + self.additional, toplot['yaxis'] if 'yaxis' in toplot else None, id='{}yaxis'.format(self.__class__.__name__)),], style={'width': '25%'}),
            html.Div([dcc.Dropdown(toplot['samplenames'] + self.additional, toplot['zaxis'] if 'zaxis' in toplot else None, id='{}zaxis'.format(self.__class__.__name__)),], style={'width': '25%'}),
            html.Div([dcc.Dropdown(toplot['samplenames'] + self.additional, toplot['colour'] if 'colour' in toplot else None, id='{}colour'.format(self.__class__.__name__)),], style={'width': '25%'}),], style={'display': 'flex'}),
            dcc.Graph(id='{}chart'.format(self.__class__.__name__), figure = c.create_3d_fig(toplot, self.config, self.defaults),
                      style={'width': '90vh', 'height': '90vh'}),
        ])

        return rtn

    def register_callbacks(self, app):
        @app.callback(Output(self.storename, 'data'),
                      [State(self.storename, 'data'),
                       Input('{}chart'.format(self.__class__.__name__), 'clickData'),
                       Input('{}testname'.format(self.__class__.__name__), 'value'),
                       Input('{}xaxis'.format(self.__class__.__name__), 'value'),
                       Input('{}yaxis'.format(self.__class__.__name__), 'value'),
                       Input('{}zaxis'.format(self.__class__.__name__), 'value'),
                       Input('{}colour'.format(self.__class__.__name__), 'value'),
                      ], prevent_initial_call=True)
        def graph_data_to_session(data, clickData, testname, xaxis, yaxis, zaxis, colour):
            storename = self.dbstorename if 'session' in self.dbstorename else self.dbstorename + '_session'

            # use data from local dcc.Store or if data is empty recover from session (if available)
            toplot = self.create_update_sessionstore(data)
            if not 'testkey' in toplot:
                # likely the session data is empty so pick same default as when drawn
                toplot['testkey'] = self.defaults['testkey']

            tname = testname
            if tname is None or tname == '':
                tname = toplot['testname'] if 'testname' in toplot else None

            namechange = not tname == toplot['testname'] if 'testname' in toplot else True

            if not 'samplenames' in toplot or namechange:
                # likely the session data is empty so construct the same data as when drawn
                toplot = c.define_plot(toplot, self.config, tname, self.defaults, print if self.config['debug'] else None)

            c.update_axis(toplot, xaxis, yaxis, zaxis=zaxis, colour=colour)

            if not clickData is None:
                toplot['selectedlabel'] = c.id_to_label(clickData['points'][0]['curveNumber'], toplot)
                if 'hovertext' in clickData['points'][0]: toplot['selectedsample'] = clickData['points'][0]['hovertext']

            return {storename: toplot}

        @app.callback(Output('{}chart'.format(self.__class__.__name__), 'figure'), [
            Input('session_store', 'data'),
            Input('{}testname'.format(self.__class__.__name__), 'value'),
            Input('{}xaxis'.format(self.__class__.__name__), 'value'),
            Input('{}yaxis'.format(self.__class__.__name__), 'value'),
            Input('{}zaxis'.format(self.__class__.__name__), 'value'),
            Input('{}colour'.format(self.__class__.__name__), 'value'),])

        def update_chart(data, testname, xaxis, yaxis, zaxis, colour):
            storename = self.dbstorename if 'session' in self.dbstorename else self.dbstorename + '_session'
            ctxid = ctx.triggered_id if not None else ''
            namechange = True if ctxid == '{}testname'.format(self.__class__.__name__) else False

            # use data from local dcc.Store or if data is empty recover from session (if available)
            doc = data[storename] if storename in data else {}
            tname = doc['testname'] if 'testname' in doc else self.defaults['testname']
            tname = tname if testname is None else testname

            toplot = c.define_plot(doc, self.config, tname, self.defaults, getdata=True) #, logfn = print)

            add_additional_plots(toplot, self.additional, xaxis, yaxis, colour = colour)

            if c.update_axis(toplot, xaxis, yaxis, zaxis=zaxis, colour=colour) and not namechange:
                raise PreventUpdate

            return c.create_3d_fig(toplot, self.config, self.defaults)

        @app.callback(Output('{}xaxis'.format(self.__class__.__name__), 'options'), [Input('session_store', 'data'), State('{}xaxis'.format(self.__class__.__name__), 'options')])
        @app.callback(Output('{}yaxis'.format(self.__class__.__name__), 'options'), [Input('session_store', 'data'), State('{}yaxis'.format(self.__class__.__name__), 'options')])
        @app.callback(Output('{}zaxis'.format(self.__class__.__name__), 'options'), [Input('session_store', 'data'), State('{}zaxis'.format(self.__class__.__name__), 'options')])
        @app.callback(Output('{}colour'.format(self.__class__.__name__), 'options'), [Input('session_store', 'data'), State('{}colour'.format(self.__class__.__name__), 'options')])
        def update_options(data, samplenames):
            storename = self.dbstorename if 'session' in self.dbstorename else self.dbstorename + '_session'

            # use data from local dcc.Store or if data is empty recover from session (if available)
            doc = data[storename] if storename in data else {}
            if 'samplenames' in doc and samplenames == doc['samplenames']:
                raise PreventUpdate

            return doc['samplenames'] if 'samplenames' in doc else []

def add_additional_plots(toplot, additional, xaxis, yaxis, zaxis = None, colour = None):
    if xaxis in additional:
        toplot['dont_plot'] = [ a for a in toplot['dont_plot'] if a != xaxis ]
        toplot['samplenames'].append(xaxis)
    if yaxis in additional:
        toplot['dont_plot'] = [ a for a in toplot['dont_plot'] if a != yaxis ]
        toplot['samplenames'].append(yaxis)
    if not zaxis is None and zaxis in additional:
        toplot['dont_plot'] = [ a for a in toplot['dont_plot'] if a != zaxis ]
        toplot['samplenames'].append(zaxis)
    if not colour is None and colour in additional:
        toplot['dont_plot'] = [ a for a in toplot['dont_plot'] if a != colour ]
        toplot['samplenames'].append(colour)

    for l in additional:
        if l in [xaxis, yaxis, zaxis, colour]:
            continue
        if not l in toplot['dont_plot']:
            toplot['dont_plot'].append(l)

graph = ThreeDPlot()
def config(doc):
    graph.config = doc

def layout():
    return graph.layout

def register_stores():
    return graph.register_stores

def register_callbacks(app):
    return graph.register_callbacks(app)

if __name__ == '__main__':
    # using custom style sheet https://dash.plotly.com/external-resources
    # folder ./static/ contains .css / .js files loaded in alphanumerical order by filename
    app = Dash(__name__, assets_folder='../static')

    app.layout = layout()

    register_callbacks(app)

    app.run_server(use_reloader=True, debug=True)
