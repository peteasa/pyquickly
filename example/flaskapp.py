#!/usr/bin/env python3

from bson.objectid import ObjectId
import dash
from dash import Dash, dcc, html, Input, Output, State
import dash_bootstrap_components as dbc
import pyquickly.database as db
from flask import session
import pyquickly.flasksession as flsk
from pyquickly.graphdata import combine_dict, combine_filter_dict
from inspect import getmembers, isfunction
import numpy as np
import sys

def log_dump(fn, doc, logfn):
    name = None
    name = doc['name'] if 'name' in doc else name
    logfn('{} {} {}'.format(fn, name, doc.keys()))

def top_layout(page_stores):
    store_default = {}
    storage_type = 'memory'
    return html.Div([
        dcc.Location(id='url_top', refresh=False),
        html.Div([dcc.Store(data=store_default, id='session_store', storage_type=storage_type)] + [
            dcc.Store(id=i, data=store_default, storage_type=storage_type) for i in page_stores]),
        html.Div(id='navigation'),
        html.Div(children='', id='session_store_display'),
        html.Hr(),
        dash.page_container,
	])

def nav_layout(dash_path):
    top_path = False
    if not dash_path == '':
        top_path = True

    return [
        dbc.Row([
            dbc.Col(['Page navigation: ', html.A('top: /', href='/') if top_path else ''] + [
                dcc.Link(
                    f"{page['name']}: "+dash_path+f"{page['path']}", href=page["relative_path"],
                    style={"marginLeft": "15px"}
                )
                for page in dash.page_registry.values()
            ]),
        ]),
    ]

def register_callbacks(config, app, dash_path, page_stores, logfn = None):

    @app.callback(Output('navigation', 'children'), [Input('url_top', 'href'), Input('url_top', 'pathname'), Input('url_top', 'search'), Input('url_top', 'hash')])
    def on_layout(href, pathname, search, hasht):
        rtn = [html.P(['Please ', html.A('click on this link', href='/'),
                       ' to create a session and enable this page'], style={'color':'red'})]
        if type(config['server']) is bool:
            rtn = nav_layout(dash_path)
        elif 'username' in session:
            rtn = nav_layout(dash_path)
            if config['debug']: rtn += [dbc.Row(['{} owns session {}'.format(session['username'], session['session_id'])])]

        return rtn

    if 0 < len(page_stores):
        @app.callback(Output('session_store', 'data'), [Input(k, page_stores[k]) for k in page_stores])
        def to_session_store(*args):
            rtn = None
            if type(config['server']) is bool:
                rtn = to_session_store_simple(args)
            else:
                rtn = to_session_store_with_mongo(args)

            return rtn

    if config['debug']:
        @app.callback(Output('session_store_display', 'children'), [Input('session_store', 'data'),])
        def on_save(data):
            return 'session store contains: {}'.format(data)

    def to_session_store_simple(args):
        rtn = {}
        for arg in args:
            rtn = combine_dict(rtn, arg)

        return rtn
    def to_session_store_with_mongo(args):
        rtn = {}
        with db.Client(config) as client:
            dbase = client[config['dbname']]
            col = dbase[config['sescol']]
            sessionrtn = col.find_one({'_id': ObjectId(session['session_id']), 'name': session['username']})
            if sessionrtn is None: sessionrtn = {'_id': session['session_id'], 'name': session['username']}
            else: sessionrtn['_id'] = str(sessionrtn['_id'])

            updated = False
            for arg in args:
                rtn, sessionrtn, updated = combine_filter_dict(rtn, sessionrtn, arg, keyfilter = 'session', updated = updated)

            if updated:
                if not logfn is None and config['debug']: logfn('flaskapp callback: updated session_store', sessionrtn)
                db.overwrite(col, sessionrtn)

            rtn, sessionrtn, updated = combine_filter_dict(rtn, {}, sessionrtn)

        return rtn

def quadratic(value, minv, maxv):

    c = np.random.choice(range(minv*maxv, maxv*maxv, 1))
    b = np.random.choice(range(minv, maxv, 1))
    m = np.random.choice(range(minv, maxv, 1))
    o = np.random.choice(range(value.min(), value.max(), 1))
    v = (value - o)

    y = (m * v * v + b * v + c) / (maxv * maxv)

    return y

def test_generate(sigfreq, numbers, divis = 1., pair = False):
    samples = np.arange(numbers) / float(divis)

    y = np.sin(2.0 * np.pi * samples * sigfreq)
    if pair:
        y = np.column_stack((y, samples))

    return y

def test_fill(config, dname, tname):
    import bson

    colname = config['dbcol']

    maxn = int(10)
    minn = int(maxn / 10)
    freq = np.random.choice(range(minn, maxn, 1))
    times = 10
    numbers = times * freq
    sample = np.arange(numbers)

    samplelabels = []
    for n in sample:
        samplelabels.append('sample_{}'.format(n))

    for n in sample:
        nums = times * np.random.choice(range(minn, maxn, 1))
        smpls = np.arange(nums)
        x = quadratic(smpls, minn * times, maxn * times)
        sf = x[n % nums]
        sf = sf * 10 if sf < 1. else sf
        sf = sf * 10 if sf < 1. else sf
        y = test_generate(int(sf), numbers = nums, divis = nums)
        data = {'name': samplelabels[n], samplelabels[n]: bson.encode({'sample': smpls.tolist(), 'x': x.tolist(), 'y': y.tolist()})}
        with db.Client(config) as client:
            dbase = client[config['dbname']]
            col = dbase[colname]

            # replacing old data with the new
            old = col.find_one({'name': data['name']})
            if old is None:
                _id = db.createId()
                old = {'_id': _id}
            else: old['_id'] = str(old['_id'])

            data = combine_dict(old, data)
            db.overwrite(col, data)

    samples = np.arange(numbers)
    x = quadratic(samples, minn * times, maxn * times)
    sigfreq = np.random.choice(range(minn, maxn, 1))
    z = test_generate(sigfreq, numbers, divis = numbers)
    sigfreq = np.random.choice(range(minn, maxn, 1))
    y = test_generate(sigfreq, numbers, divis = numbers)
    sigfreq = np.random.choice(range(minn, maxn, 1))
    x = test_generate(sigfreq, numbers, divis = numbers)
    sigfreq = np.random.choice(range(minn, maxn, 1))
    w = test_generate(sigfreq, numbers, divis = numbers)

    data = {'name': dname, tname: bson.encode({'sample': sample.tolist(), 'samplelabels': samplelabels,
                                                   'w': w.tolist(), 'x': x.tolist(), 'y': y.tolist(), 'z': z.tolist()})}

    return data

def test_preparation(config, did = None):
    colname = config['dbcol']
    dname = config['testset']
    tname = config['testname']
    data = test_fill(config, dname, tname)

    with db.Client(config) as client:
        dbase = client[config['dbname']]
        col = dbase[colname]

        # replacing old data with the new
        old = col.find_one({'name': data['name']})
        if old is None:
            _id = db.createId() if did is None else did
            old = {'_id': _id}
        else: old['_id'] = str(old['_id'])

        data = combine_dict(old, data)
        print('test_preparation', data.keys())
        db.overwrite(col, data)

if __name__ == '__main__':
    DASH_ROOT = '/content'
    MONGODB = 'flaskapp'
    TESTSESSION = 'tsession'
    TESTCOLLECTION = 'tstore'
    TESTSET = 'tset'
    TESTNAME = '4 sin'
    config = {'dbaddr': 'localhost', 'dbport': 27017, 'dbname': MONGODB,
              'sescol': TESTSESSION,
              'dbcol': TESTCOLLECTION, 'testset': TESTSET, 'testname': TESTNAME, 'debug': False}

    server_config = {'dbaddr': config['dbaddr'], 'dbport': config['dbport'], 'dbname': config['dbname'],
                     'sescol': config['sescol'],
                     'dash_root': '/content', 'server': True, 'debug': False}

    testdata = db.read(config, config['dbcol'], {'name': config['testset']})
    if testdata is None: test_preparation(config)
    flsksession = None
    dash_path = ''
    if 1:
        flsksession = flsk.Session(server_config, server_config['sescol'], server_config['dash_root']+'/')
        server_config['server'] = flsksession.server
        dash_path = server_config['dash_root']

    app = dash.Dash(__name__, server=server_config['server'], url_base_pathname=dash_path+'/',
                    assets_folder='static', pages_folder='content', use_pages=True) #, suppress_callback_exceptions=True)

    page_register_fns = []
    page_stores = {}
    for page in dash.page_registry:
        fn = getattr(sys.modules[page], 'config', False)
        if fn:
            fn(config)

        fn = getattr(sys.modules[page], 'register_callbacks', False)
        if fn:
            page_register_fns.append(fn)

        fn = getattr(sys.modules[page], 'register_stores', False)
        if fn:
            page_stores = combine_dict(page_stores, fn())

    app.layout = top_layout(page_stores)
    for fn in page_register_fns:
        fn(app)

    register_callbacks(server_config, app, dash_path, page_stores) #, logfn = print)

    app.run(use_reloader = True, debug = server_config['debug'])
