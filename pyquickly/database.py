#!/usr/bin/env python3

from bson.objectid import ObjectId
from pymongo import MongoClient

class Client(object):
    def __init__(self, config):
        self._addr = config['dbaddr']
        self._port = config['dbport']
        self._client = None

    def __getitem__(self, key):
        rtn = None if self._client is None else self._client[key]
        return rtn

    def __enter__(self):
        self.open()

        return self

    def __exit__(self, a, b, c):
        # ref https://docs.python.org/3/reference/datamodel.html?highlight=__exit__#object.__exit__
        if not a is None:
            print("a", a)
        if not b is None:
            print("b", b)
        if not c is None:
            print("c", c)

        self.close()

    @property
    def server_info(self):
        return self._client.server_info

    def close(self):
        self._client.close()
        self._client = None

    def open(self):
        self._client = MongoClient(self._addr, self._port)

        return self

def get_id(col, doc_key, logfn = None):
    _id = None
    ret_doc = None
    if '_id' in doc_key:
        ret_doc = doc_key
    elif 'name' in doc_key:
        # find a named individual
        ret_doc = col.find_one({'name': doc_key['name']})

    if ret_doc is None:
        # always return a valid document
        ret_doc = doc_key

    if '_id' in ret_doc:
        _id = ret_doc['_id']

    return _id, ret_doc

def find(col, doc_key, logfn = None):
    _id, ret_doc = get_id(col, doc_key, logfn)
    if not logfn is None:
        log_dump('found', ret_doc, logfn)

    if '_id' in ret_doc and type(ret_doc['_id']) is ObjectId:
        ret_doc['_id'] = str(ret_doc['_id'])

    return ret_doc

def delete_if_present(col, doc_key, logfn = None):
    _id, ret_doc = get_id(col, doc_key, logfn)
    if not _id is None:
        if not type(_id) is ObjectId:
            _id = ObjectId(_id)

        res = col.delete_one({'_id': _id})
        if not logfn is None:
            logfn('Delete {} result: {}'.format(doc_key['name'], doc_key, res.raw_result))

    if '_id' in ret_doc and type(ret_doc['_id']) is ObjectId:
        ret_doc['_id'] = str(ret_doc['_id'])

    return ret_doc

def overwrite(col, doc, logfn = None):
    ret_doc = delete_if_present(col, doc, logfn)
    if not logfn is None:
        log_dump('overwrite', ret_doc, logfn)

    if '_id' in ret_doc:
        _id = ret_doc['_id']
        if not type(_id) is ObjectId:
            _id = ObjectId(_id)
        doc['_id'] = _id
    else:
        _id = ObjectId()
        doc['_id'] = _id

    gather_id = col.insert_one(doc).inserted_id
    doc['_id'] = str(doc['_id'])

def createId():
    return str(ObjectId())

def constructId(_id):
    return ObjectId(_id)

def read(config, collection, docref):
    mongodata = {}
    if 0 < len(docref):
        with MongoClient(config['dbaddr'], config['dbport']) as client:
            db = client[config['dbname']]
            col = db[collection]
            mongodata = col.find_one(docref)
            if not mongodata is None: mongodata['_id'] = str(mongodata['_id'])

    return mongodata

def delete(config, collection, mongodata):
    rtn = None
    with MongoClient(config['dbaddr'], config['dbport']) as client:
        db = client[config['dbname']]
        col = db[collection]
        rtn = delete_if_present(col, mongodata)

    return rtn
