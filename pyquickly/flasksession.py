#!/usr/bin/env python3

import pyquickly.database as db
from flask import Flask, session, request, redirect, url_for

class Session(object):
    def __init__(self, config, dbcol, url_for_app):
        self.config = config
        self.dbcol = dbcol
        self.url_for_app = url_for_app
        self._server = Flask(__name__)
        self._server.secret_key = 'timetogotobed'
        self.manage()

    @property
    def server(self):
        return self._server

    def open(self, username):
        session['username'] = username
        session['session_id'] = db.createId()

    def close(self):
        if 'username' in session and not session['username'] is None and not session['username'] == '':
            # delete mongo store
            db.delete(self.config, self.dbcol,
                      {'_id': db.constructId(session['session_id']), 'name': session['username']})

        session.pop('username', None)
        session.pop('session_id', None)

    def look_in_store(self, name):
        dbstore = ''
        dbsessiondata = db.read(self.config, self.dbcol, {'name': name})
        if not dbsessiondata is None:
            dbstore = '''<p>Saved session: {} _id {} contains: {}</p>'''.format(dbsessiondata['name'],
                                                                               dbsessiondata['_id'],
                                                                               dbsessiondata.keys())

        return dbstore, dbsessiondata

    def check_existing(self, username):
        dbstore, dbsessiondata = self.look_in_store(username)
        if dbsessiondata is None:
            self.open(username)
            rtn = redirect(url_for(self.url_for_app))
        else:
            rtn = start_new(name = username, new = ' new')
            rtn = re_use_stored(rtn, dbstore, dbsessiondata)

        return rtn

    def manage(self):
        @self._server.route('/', methods = ['GET', 'POST'])
        def index():
            message1 = 'Enter a name to start session'
            button = 'Press to create'
            name = ''
            if 'username' in session:
                username = session['username']
                if username is None or username == '':
                    self.close()
                else:
                    message1 = 'Enter a new name or continue with existing session'
                    button = 'Press to continue'
                    name = session['username']

            rtn = '''{}
            <form action = "" method = "post">
            <p><input type = "text" name = "username" value = "{}"/></p>
            <p><input type = "submit" name = 'btn' value = "{}"/></p>
            </form>
            '''.format(message1, name, button)
            dbstore, dbsessiondata = self.look_in_store(name)
            if request.method == 'GET' and 'username' in session:
                rtn = rtn+'''Logged in as {}
                <form action = "" method = "post">
                <p><input type = "submit" name = 'btn' value = "Press to clear name and session"/></p>
                </form>
                '''.format(name)
                if not dbsessiondata is None:
                    rtn = re_use_stored(rtn, dbstore, dbsessiondata)

            elif request.method == 'POST' and request.form.get('btn') == "Press to continue":
                username = request.form.get('username').replace(' ', '')
                if username == '' or username == "":
                    rtn = start_new()
                elif not username == dbsessiondata['name']:
                    self.open(username)
                    rtn = redirect(url_for(self.url_for_app))
                else:
                    rtn = redirect(url_for(self.url_for_app))

            elif request.method == 'POST' and request.form.get('btn') == 'Press to re-use':
                username = request.form.get('username').replace(' ', '')
                if not username == '':
                    dbstore, dbsessiondata = self.look_in_store(username)

                if not username == '' and not dbsessiondata is None:
                    session['username'] = dbsessiondata['name']
                    session['session_id'] = dbsessiondata['_id']
                    rtn = redirect(url_for(self.url_for_app))
                else:
                    rtn = start_new()
                    rtn = re_use_stored(rtn, dbstore, dbsessiondata)

            elif request.method == 'POST' and request.form.get('btn') == 'Press to create':
                username = request.form.get('username').replace(' ', '')
                if username == '':
                    rtn = start_new()
                else:
                    rtn = self.check_existing(username)

            elif request.method == 'POST' and request.form.get('btn') == 'Press to create new':
                username = request.form.get('username').replace(' ', '')
                if not username == '':
                    self.open(username)
                    rtn = redirect(url_for(self.url_for_app))
                else:
                    rtn = start_new()
                    rtn = re_use_stored(rtn, dbstore, dbsessiondata)
            elif request.method == 'POST' and request.form.get('btn') == 'Press to clear name and session':
                name = ''
                if 'username' in session and not session['username'] is None and not session['username'] == '':
                    name = session['username']

                self.close()
                dbstore, dbsessiondata = self.look_in_store(name)
                if not dbsessiondata is None and 'name' in dbsessiondata:
                    session['username'] = dbsessiondata['name']
                    session['session_id'] = dbsessiondata['_id']
                    rtn = dbstore
                    rtn += '''<form action = "" method = "post">
                    <p><input type = "submit" name = 'btn' value = "Press to clear name and session"/></p>
                    </form>
                    '''
                    rtn += '''<form action = "" method = "post">
                    <p><input type = "text" name = "username" value = "{}"/></p>
                    <p><input type = "submit" name = 'btn' value = "Press to continue"/></p>
                    </form>
                    '''.format(dbsessiondata['name'])
                else:
                    rtn = start_new(name = name)

            return rtn

def re_use_stored(rtn, dbstore, dbsessiondata):
    if not dbsessiondata is None:
        rtn += dbstore
        rtn += '''Re-use saved session
        <form action = "" method = "post">
        <p><input type = "text" name = "username" value = "{}" readonly style="background-color: lightgrey;"/></p>
        <p><input type = "submit" name = "btn" value = "Press to re-use"/></p>
        </form>'''.format(dbsessiondata['name'])
    return rtn

def start_new(name = '', new = ''):
    return '''Enter a name to start session
    <form action = "" method = "post">
    <p><input type = "text" name = "username" value = "{}" /></p>
    <p><input type = "submit" name = 'btn' value = "Press to create{}"/></p>
    </form>'''.format(name, new)
