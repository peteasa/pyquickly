#!/usr/bin/env python3

def combine_create_dict(rtn, arg, k):
    if type(arg[k]) is dict:
        if not k in rtn:
            rtn[k] = {}
        for j in arg[k]:
            rtn[k][j] = arg[k][j]
    else: rtn[k] = arg[k]

def combine_filter_dict(rtn, sessionrtn, arg, keyfilter = None, updated = False, logfn = None):
    if not logfn is None: logfn('combine_dict rtn {} sessionrtn {} arg {}'.format(rtn, sessionrtn, arg))
    if not arg is None:
        for k in arg:
            if not logfn is None: logfn('combine_dict key {} data {}'.format(k, arg[k]))
            if not arg[k] is None:
                combine_create_dict(rtn, arg, k)
                if not keyfilter is None and keyfilter in k:
                    if not (k in sessionrtn and arg[k] == sessionrtn[k]):
                        if not logfn is None: logfn('flaskapp combine_filter_dict update {} {}'.format(k, arg[k]))
                        combine_create_dict(sessionrtn, arg, k)
                        updated = True

    if not logfn is None: logfn('combine_filter_dict DONE rtn {} sessionrtn {}'.format(rtn, sessionrtn))

    return rtn, sessionrtn, updated

def combine_dict(rtn, arg, logfn = None):
    if not logfn is None: logfn('combine_dict rtn {} arg {}'.format(rtn, arg))
    rtn, sessionrtn, updated = combine_filter_dict(rtn, {}, arg, logfn = logfn)

    return rtn
