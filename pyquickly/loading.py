#!/usr/bin/env python3

"""
Provides a workround for page modules not imported correctly https://github.com/plotly/dash/issues/2312
"""

import importlib
import os

def walk_pages_folder(pages_folder, method_name):
    methods = {}
    walk_dir = pages_folder
    for (root, dirs, files) in os.walk(walk_dir):
        dirs[:] = [
            d for d in dirs if not d.startswith(".") and not d.startswith("_")
        ]
        for file in files:
            if (
                    file.startswith("_")
                    or file.startswith(".")
                    or not file.endswith(".py")
            ):
                continue

            with open(os.path.join(root, file), encoding="utf-8") as f:
                content = f.read()
                if "register_page" not in content:
                    continue

            page_filename = os.path.join(root, file).replace("\\", "/")
            _, _, page_filename = page_filename.partition(
                pages_folder.replace("\\", "/") + "/"
            )
            page_filename = page_filename.replace(".py", "").replace("/", ".")

            pages_folder = (
                pages_folder.replace("\\", "/").lstrip("/").replace("/", ".")
            )

            module_name = ".".join([pages_folder, page_filename])

            spec = importlib.util.spec_from_file_location(
                module_name, os.path.join(root, file)
            )

            page_module = importlib.util.module_from_spec(spec)
            print("functions before load", getmembers(page_module, isfunction))
            try:
                spec.loader.exec_module(page_module)
                #print("loading: loaded", module_name)
            except dash.exceptions.PageError:
                pass #print("loading: error load", module_name)

            #print("loading: file", file, "module_name ", module_name, "spec", spec, "page_module ", page_module)
            fn = getattr(
                page_module, method_name, False
            )
            if fn:
                methods[module_name] = fn
                #print("loading: module_name", module_name, fn)

            #print("functions", getmembers(page_module, isfunction))

    return methods
