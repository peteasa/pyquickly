#!/usr/bin/env python3

import base64
import bson
import bsonjs
import pyquickly.database as db
from flask import session
import json
from math import log10, floor
import numpy as np
import pandas as pd
import plotly.graph_objects as go

# python -m pip install --user python-bsonjs

class ChartPlotter(object):
    def __init__(self, auxiliarynames = None, logfn = None):
        # only store static config in the global object
        self._config = {}

        # only store defaults in the global object
        self._defaults = {}

        self._fallback_defaults = None
        self._internal_aux_names = ['_id',
                                    'testkey', 'testnames', 'testname', 'samplenames',
                                    'xaxis', 'yaxis', 'zaxis', 'colour',
                                    'labelname', 'selectedlabel', 'selectedsample',
                                    'dont_plot', 'auxnames', 'keep'
        ]

        self.keep = []
        if not auxiliarynames is None:
            # auxiliary names are returned in a record by a database read
            # they can be useful for creating a future database search
            self.keep = auxiliarynames
            _update_name_list(self._internal_aux_names, auxiliarynames)

        self.logfn = logfn

    def __str__(self):
        return '{} defaults {}'.format(self.__class__.__name__, self.defaults.keys())

    @property
    def register_stores(self):
        rtn = {'{}'.format(self.storename): 'data'}
        return rtn

    @property
    def config(self):
        return self._config
    @config.setter
    def config(self, doc):
        self._config = doc

    @property
    def fallback_defaults(self):
        return self._fallback_defaults
    @fallback_defaults.setter
    def fallback_defaults(self, doc):
        if ('testnames' in doc and 'testname' in doc and
            'samplenames' in doc and doc['testname'] in doc):
            self._fallback_defaults = doc

        self._fallback_defaults['auxnames'] = []
        _update_name_list(self._fallback_defaults['auxnames'], self._fallback_defaults['testkey'])
        _update_name_list(self._fallback_defaults['auxnames'], self._internal_aux_names)
        if 'keep' in self._fallback_defaults:
            _update_name_list(self._fallback_defaults['keep'], self.keep)
        elif 0 < len(self.keep):
            self._fallback_defaults['keep'] = self.keep

    @property
    def defaults(self):
        if self._defaults == {}:
            # need to be within a session for create_update_sessionstore to operate
            testname = None
            if 'testname' in self.fallback_defaults: testname = self.fallback_defaults['testname']
            self._defaults = self.define_plot(testname = testname, getdata=True, logfn = self.logfn)

        return self._defaults

    def create_update_sessionstore(self, data):
        toplot = {}
        session_store_name = self.dbstorename if 'session' in self.dbstorename else self.dbstorename + '_session'
        if self.dbstorename in data:
            toplot = data[self.dbstorename]
        elif session_store_name in data:
            toplot = data[session_store_name]
        elif 'username' in session:
            dbsessiondata = db.read(self.config, self.config['sescol'],
                                    {'_id': db.constructId(session['session_id']), 'name': session['username']})
            if not dbsessiondata is None and session_store_name in dbsessiondata:
                toplot = dbsessiondata[session_store_name]

        return toplot

    def define_plot(self, testname = None, defaults = None, getdata = False, logfn = None):
        '''Fetch data to plot from session store or use defaults'''
        toplot = self.create_update_sessionstore({})

        if 'testname' in toplot:
            testname = toplot['testname']
        elif 'testnames' in toplot and 0 < len(toplot['testnames']):
            testname = toplot['testnames'][0]

        defaults = self.fallback_defaults if defaults is None else defaults

        return define_plot(toplot, self.config, testname, defaults, getdata=getdata, logfn = logfn if not logfn is None else self.logfn)

def _update_name_list(auxiliarynames, newauxiliarynames):
    for name in newauxiliarynames:
        if not name in auxiliarynames:
            auxiliarynames.append(name)

    return auxiliarynames

def get_testnames(doc, auxiliarynames):
    testnames = []
    for k in doc:
        if not k in auxiliarynames:
            if not type(doc[k]) is list:
                testnames.append(k)

    return testnames

def get_testname(toplot, testname):
    tname = None
    if not testname is None:
        tname = testname
        if 'testname' in toplot and not tname == toplot['testname']:
            # sample names need to be changed because the selected test may have different numbers of plots
            del toplot['samplenames']
    elif 'testname' in toplot:
        tname = toplot['testname']
    elif 'testnames' in toplot and 0 < len(toplot['testnames']):
        tname = toplot['testnames'][0]
        del toplot['samplenames']

    return tname

def create_dont_plot(doc):
    dont_plot = []
    if 'dont_plot' in doc:
        dont_plot = doc['dont_plot']

    return dont_plot

def define_plot(toplot, dbconfig, testname, defaults, _dont_plot = None, getdata=False, logfn = None):
    '''Generate data for plotting on a chart from toplot or database or defaults'''
    if not logfn is None: logfn('plotting define_plot testname {} getdata {} toplot {}'.format(testname, getdata, toplot.keys()))

    dont_plot = _dont_plot
    if _dont_plot is None:
        dont_plot = create_dont_plot(toplot) if 'dont_plot' in toplot else create_dont_plot(defaults)

    testkey = toplot['testkey'] if 'testkey' in toplot else defaults['testkey']
    if 'testkey' in toplot:
        for key in testkey:
            if 'testkey' in toplot and key in toplot['testkey'] and not toplot['testkey'][key] == testkey[key]:
                if 'samplenames' in toplot: del toplot['samplenames']
            elif 'testkey' in toplot and not key in toplot['testkey']:
                if 'samplenames' in toplot: del toplot['samplenames']
    else:
        if 'samplenames' in toplot: del toplot['samplenames']

    auxnames = toplot['auxnames'] if not getdata and 'auxnames' in toplot else defaults['auxnames']
    _update_name_list(auxnames, [k for k in testkey.keys()])
    keep = toplot['keep'] if 'keep' in toplot else []
    if not 'keep' in toplot and 'keep' in defaults:
        _update_name_list(keep, defaults['keep'])

    labelname = None
    if 'labelname' in toplot:
        labelname = toplot['labelname']
    elif 'labelname' in defaults:
        labelname = defaults['labelname']

    if not 'samplelabels' in dont_plot:
        dont_plot.append('samplelabels')
    if not labelname is None:
        if not labelname in dont_plot:
            dont_plot.append(labelname)

    tname = get_testname(toplot, testname)
    tnames = toplot['testnames'] if 'testnames' in toplot else get_testnames(toplot, auxnames)
    doc = {}
    if (getdata
        or
        (not 'samplenames' in toplot)
        or
        ('samplenames' in toplot and len(toplot['samplenames']) < 1)
        or
        tname is None
        or
        (not tname is None and not tname in toplot)):
        if 0 < len(dbconfig):
            if not logfn is None: logfn('plotting define_plot Using raw test samples from {} getdata {}'.format(testkey, getdata))
            doc = db.read(dbconfig, dbconfig['dbcol'], testkey)

        if doc is None:
            print('ERROR: plotting define_plot fetch failed for {}'.format(testkey))
            if not logfn is None: logfn('ERROR: plotting define_plot fetch failed for {}'.format(testkey))
            doc = defaults

        tnames = get_testnames(doc, auxnames)
        if not logfn is None: logfn('plotting define_plot db read testkey {} testname {} doc {}'.format(testkey, tname, doc.keys()))

    # at this point expect at least testset, and one testname to be defined
    if tname is None or tname == '':
        tname = tnames[0]
        if 'testname' in defaults and defaults['testname'] in tnames:
            tname = defaults['testname']

        print('WARNING: plotting define_plot using testname {} {}'.format(tname, tnames))

    data = None
    if 0 < len(doc) and tname in doc:
        try:
            if not logfn is None:
                if type(doc[tname]) is dict and '$binary' in doc[tname] and 'subType' in doc[tname] :
                    logfn('define_plot doc entry $binary keys {} subType {}'.format(doc[tname]['$binary'].keys(), doc[tname]['$binary']['subType']))
                elif type(doc[tname]) is dict and '$binary' in doc[tname] :
                    logfn('doc entry $binary keys are {}'.format(doc[tname]['$binary'].keys()))
                elif type(doc[tname]) is dict:
                    logfn('doc entry keys are {}'.format(doc[tname].keys()))

            if type(doc[tname]) is dict and '$binary' in doc[tname] and doc[tname]['$binary']['subType'] == '00':
                # https://bsonspec.org/spec.html
                #  libbson-1.0-0/jammy 1.21.0-1build1 amd64 uses this
                data = base64.b64decode(doc[tname]['$binary']['base64'])
                data = json.loads(bsonjs.dumps(data))
            else:
                # libbson-1.0-0/focal 1.16.1-1build2 amd64 uses this
                data = bson.decode(doc[tname])

            if not logfn is None: logfn('plotting define_plot found doc[{}] {}'.format(tname, data.keys()))
        except TypeError:
            print('WARNING: plotting define_plot using fallback_defaults doc[{}] {}'.format(tname, tnames))
            if not logfn is None: logfn('WARNING: plotting define_plot using fallback_defaults doc[{}] {}'.format(tname, tnames))
            data = doc[tname]
    elif tname in toplot:
        data = toplot[tname]
    else:
        tname = defaults['testname']
        data = defaults[defaults['testname']]

    axisnames = []
    samplenames = []
    for k in data:
        if not k in dont_plot:
            samplenames.append(k)
            if not k in ['sample']:
                axisnames.append(k)

    if not logfn is None and 0 < len(samplenames):
        if samplenames[0] in data:
            logfn('plotting define_plot {} {} #samples {}'.format(testkey, tname, len(data[samplenames[0]])))

    rtn = {'testkey': testkey, 'testnames': tnames, 'testname': tname,
           'samplenames': samplenames,
           'auxnames': auxnames}

    for k in keep:
        if 0 < len(doc):
            if k in doc:
                rtn[k] = doc[k]
        else:
            if k in toplot:
                rtn[k] = toplot[k]

    if not labelname is None:
        rtn['labelname'] = labelname

    if 0 < len(dont_plot):
        rtn['dont_plot'] = dont_plot

    if 0 < len(keep):
        rtn['keep'] = keep

    if getdata:
        rtn[tname] = data

    # preference for existing x,y,z,colour settings
    xaxis = toplot['xaxis'] if 'xaxis' in toplot else (defaults['xaxis'] if 'xaxis' in defaults and defaults['xaxis'] in samplenames + ['sample'] else None)
    yaxis = toplot['yaxis'] if 'yaxis' in toplot else (defaults['yaxis'] if 'yaxis' in defaults and defaults['yaxis'] in samplenames else None)
    zaxis = toplot['zaxis'] if 'zaxis' in toplot else (defaults['zaxis'] if 'zaxis' in defaults and defaults['zaxis'] in samplenames else None)
    # preference for x,y,z, colour settings that exist within this testname
    xaxis = xaxis if xaxis in samplenames + ['sample'] else (defaults['xaxis'] if 'xaxis' in defaults and defaults['xaxis'] in samplenames + ['sample'] else None)
    yaxis = yaxis if xaxis in samplenames else (defaults['yaxis'] if 'yaxis' in defaults and defaults['yaxis'] in samplenames else None)
    zaxis = zaxis if xaxis in samplenames else (defaults['zaxis'] if 'zaxis' in defaults and defaults['zaxis'] in samplenames else None)

    # if no axis is specified introduce 'sample'
    if xaxis is None and yaxis is None and zaxis is None: xaxis = 'sample'

    colour = toplot['colour'] if 'colour' in toplot else (defaults['colour'] if 'colour' in defaults and defaults['colour'] in samplenames else None)
    colour = colour if colour in samplenames and not colour == 'no-color' else (defaults['colour'] if 'colour' in defaults and defaults['colour'] in samplenames else None)

    if not xaxis is None: rtn['xaxis'] = xaxis
    if not yaxis is None: rtn['yaxis'] = yaxis
    if not zaxis is None: rtn['zaxis'] = zaxis
    rtn['colour'] = colour # either one of samplenames or None or 'no-colour'

    if not logfn is None: logfn('plotting define_plot DONE {} axes: {} {} {} {}'.format(rtn.keys(), xaxis, yaxis, zaxis, colour))

    return rtn

def derive_colour(colour, samplenames, x, y, z='2d-plot'):
    n_samplenames = -1 if 'sample' in samplenames else 0
    n_samplenames += len(samplenames)
    if x == 'sample' or y == 'sample':
        if z == '2d-plot': n_samplenames += 1
        elif z == 'sample': n_samplenames += 1

    available_for_colour = n_samplenames
    if x is None or y is None or z is None:
        available_for_colour = 0

    available_for_colour = available_for_colour - (2 if z == '2d-plot' else 3)

    if 0 < available_for_colour and colour is None:
        tofind = 2 if z == '2d-plot' else 3
        col = None
        fstcol = None
        for n in samplenames:
            if tofind == 0: col = n; break
            if n == x: tofind -= 1; continue
            if n == y: tofind -= 1; continue
            if n == z: tofind -= 1; continue
            if fstcol is None: fstcol = n

        col = fstcol if col is None else col
    elif colour is None or colour == 'no-colour':
        col = None
    else: col = colour

    return col, n_samplenames

def colour_bar(data):
    dmin = data.min()
    dmax = data.max()
    dstep = (dmax - dmin)/10
    delta = 10**floor(log10(dstep))
    ro = int(log10(delta))-1
    tickvals = np.arange(dmin, dmax, (dmax - dmin)/10)
    ticktext = ['{}'.format(round(i, -ro)) for i in tickvals]

    return tickvals, ticktext

def update_axis(toplot, xaxis, yaxis, zaxis = None, colour = None):
    '''Update axises checking for changes'''
    noupdate = True
    if xaxis is None:
        if 'xaxis' in toplot:
            if not toplot['xaxis'] is None: noupdate = False
            toplot['xaxis'] = xaxis
    else:
        if ('xaxis' in toplot and not toplot['xaxis'] == xaxis) or not 'xaxis' in toplot:
            toplot['xaxis'] = xaxis if xaxis in toplot['samplenames'] + ['sample'] else None
            noupdate = False
    if yaxis is None:
        if 'yaxis' in toplot:
            if not toplot['yaxis'] is None: noupdate = False
            toplot['yaxis'] = yaxis
    else:
        if ('yaxis' in toplot and not toplot['yaxis'] == yaxis) or not 'yaxis' in toplot:
            toplot['yaxis'] = yaxis if yaxis in toplot['samplenames'] else None
            noupdate = False
    if zaxis is None:
        if 'zaxis' in toplot:
            if not toplot['zaxis'] is None: noupdate = False
            toplot['zaxis'] = zaxis
    else:
        if ('zaxis' in toplot and not toplot['zaxis'] == zaxis) or not 'zaxis' in toplot:
            toplot['zaxis'] = zaxis if zaxis in toplot['samplenames'] else None
            noupdate = False
    if colour is None:
        if 'colour' in toplot:
            if not toplot['colour'] is None: noupdate = False
            toplot['colour'] = colour
    else:
        if ('colour' in toplot and not toplot['colour'] == colour) or not 'colour' in toplot:
            toplot['colour'] = colour if colour in toplot['samplenames'] else None
            noupdate = False

    return noupdate

def add_trace(fig, data, labelname, name, x, y, colour):
    text = '+text' if labelname in data else ''
    samplelabels = data[labelname] if labelname in data else None

    if not colour is None:
        tickvals, ticktext = colour_bar(data[colour])
    fig.add_trace(
        go.Scatter(x=data[x], y=data[y], name=name,
                   mode='markers', marker=(dict(colorscale='Turbo', opacity=0.8,
                   color=data[colour] if not colour is None else None,
                   colorbar=dict(title=colour, thickness=10, tickvals=tickvals, ticktext=ticktext, outlinewidth=0)) if not colour is None else None),
                   hoverinfo='x+y{}'.format(text), hovertext=samplelabels),
    )

def add_3d_trace(fig, data, labelname, name, x, y, z, colour):
    text = '+text' if labelname in data else ''
    samplelabels = data[labelname] if labelname in data else None

    if not colour is None:
        tickvals, ticktext = colour_bar(data[colour])
    fig.add_trace(
        go.Scatter3d(x=data[x], y=data[y], z=data[z], name=name,
                     mode='markers', marker=(dict(colorscale='Turbo', opacity=0.8,
                     color=data[colour] if not colour is None else None,
                     colorbar=dict(title=colour, thickness=10, tickvals=tickvals, ticktext=ticktext, outlinewidth=0)) if not colour is None else None),
                     hoverinfo='x+y+z{}'.format(text), hovertext=samplelabels,
        ),
    )

def add_ytrace(fig, data, name, y, yaxis):
    fig.add_trace(
        go.Scatter(x=[ i for i in data.index ], y=data[y], name=name, yaxis=yaxis, mode='markers'),
    )

def add_yaxis(title, colour):
    return dict(
        title=title,
        titlefont=dict(
            color=colour
        ),
        tickfont=dict(
            color=colour
        )
    )

def add_ynaxis(side, position, title, colour):
    return dict(
        title=title,
        titlefont=dict(
            color=colour
        ),
        tickfont=dict(
            color=colour
        ),
        anchor='x',  # specifying x - axis has to be the fixed
        overlaying='y',  # specifyinfg y - axis has to be separated
        side=side,  # specifying the side the axis should be present
        position=position  # specifying the position of the axis
    )

def two_axis_plot(title, data, samplenames, labelname, x, y, colour):
    fig = go.Figure()

    if not 'sample' in samplenames:
        sample = np.arange(len(data[samplenames[0]]))
        data['sample'] = sample

    col, n_samplenames = derive_colour(colour, samplenames, x, y)

    if y is None:
        col = None if 2 < n_samplenames else col
        for n in samplenames:
            if n == x: continue
            if x is None: x = 'sample'
            add_trace(fig, data, labelname, n, x, n, col)
    elif x is None:
        col = None if 2 < n_samplenames else col
        for n in samplenames:
            if n == y: continue
            if y is None: y = samplenames[-1] if 0 < n_samplenames else None
            add_trace(fig, data, labelname, n, n, y, col)
    else:
        col = col if 2 < n_samplenames else None
        add_trace(fig, data, labelname, y, x, y, col)

    X1POS = 0.
    fig.update_layout(
        title_text='{}'.format(title),
        yaxis=dict(
            title='value' if y is None else y,
            ),
        xaxis=dict(
            title='value' if x is None else x,
            titlefont=dict(
                color='#0000ff'
            ),
            tickfont=dict(
                color='#0000ff'
            ),
            position=X1POS  # specifying the position of the axis
        ),
    )

    return fig

def threed_plot(title, data, samplenames, labelname, x, y, z, colour):
    fig = go.Figure()

    if not 'sample' in samplenames:
        sample = np.arange(len(data[samplenames[0]]))
        data['sample'] = sample

    col, n_samplenames = derive_colour(colour, samplenames, x, y, z=z)

    if y is None:
        col = None if 3 < n_samplenames else col
        for n in samplenames:
            if n == x: continue
            if x is None: x = 'sample'
            if z is None: z = samplenames[-1] if 0 < n_samplenames else None
            add_3d_trace(fig, data, labelname, n, x, n, z, col)
    elif x is None:
        col = None if 3 < n_samplenames else col
        for n in samplenames:
            if n == y: continue
            if y is None: y = samplenames[-1] if 0 < n_samplenames else None
            if z is None: z = samplenames[-2] if 1 < n_samplenames else None
            add_3d_trace(fig, data, labelname, n, n, y, z, col)
    elif z is None:
        col = None if 3 < n_samplenames else col
        for n in samplenames:
            if n == z: continue
            if x is None: x = 'sample'
            if y is None: z = samplenames[-1] if 0 < n_samplenames else None
            add_3d_trace(fig, data, labelname, n, x, y, n, col)
    else:
        col = col if 3 < n_samplenames else None
        add_3d_trace(fig, data, labelname, y, x, y, z, col)

    scene = {}
    if not x is None: scene['xaxis_title'] = x
    if not y is None: scene['yaxis_title'] = y
    if not z is None: scene['zaxis_title'] = z
    if x is None: scene['xaxis_title'] = 'valx'
    if y is None: scene['yaxis_title'] = 'valy'
    if z is None: scene['zaxis_title'] = 'valz'
    if not x is None or not y is None or not z is None: fig.update_layout(scene = scene)

    return fig

def id_to_label(nameid, toplot):
    label = toplot['samplenames'][0]
    if 'xaxis' in toplot and 'yaxis' in toplot:
        # xy plot so choose one
        label = toplot['yaxis']
    else:
        index = 0
        for n in toplot['samplenames']:
            if n == 'sample': continue
            if index == nameid: label = n
            index += 1

    return label

def create_fig(doc, config, defaults, logfn = None):
    tname = ''
    if 'testname' in doc:
        tname = doc['testname']
    elif 'testname_session' in doc:
        tname = doc['testname_session']
        doc['testname'] = tname

    if not tname in doc:
        if not logfn is None: logfn('plotting create_fig testname {} doc {}'.format(tname, doc.keys()))
        doc = define_plot(doc, config, tname, defaults, getdata=True, logfn = logfn)

    tname = doc['testname']
    data = doc[tname]

    x = doc['xaxis'] if 'xaxis' in doc else None
    y = doc['yaxis'] if 'yaxis' in doc else None
    colour = doc['colour'] if 'colour' in doc else 'no-colour'
    if not logfn is None: logfn('plotting create_fig', doc.keys(), x, y)

    df = pd.DataFrame(data)

    labelname = doc['labelname'] if 'labelname' in doc else 'samplelabels'
    fig = two_axis_plot(tname, df, doc['samplenames'], labelname, x, y, colour)

    fig.update_layout(clickmode='event+select')

    return fig

def create_3d_fig(doc, config, defaults, logfn = None):
    tname = ''
    if 'testname' in doc:
        tname = doc['testname']
    elif 'testname_session' in doc:
        tname = doc['testname_session']
        doc['testname'] = tname

    if not tname in doc:
        if not logfn is None: logfn('plotting create_3d_fig testname {} doc {}'.format(tname, doc.keys()))
        doc = define_plot(doc, config, tname, defaults, getdata=True, logfn = logfn)

    tname = doc['testname']
    data = doc[tname]

    x = doc['xaxis'] if 'xaxis' in doc else None
    y = doc['yaxis'] if 'yaxis' in doc else None
    z = doc['zaxis'] if 'zaxis' in doc else None
    colour = doc['colour'] if 'colour' in doc else 'no-colour'
    if not logfn is None: logfn('plotting create_3d_fig', doc.keys(), x, y, z)

    df = pd.DataFrame(data)

    labelname = doc['labelname'] if 'labelname' in doc else 'samplelabels'
    fig = threed_plot(tname, df, doc['samplenames'], labelname, x, y, z, colour)

    fig.update_layout(clickmode='event+select')

    return fig
