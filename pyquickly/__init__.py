"""
Provides a python library to assist with the generation of dash plotly (https://github.com/plotly/dash) graph applications
"""

from .version import __version__
